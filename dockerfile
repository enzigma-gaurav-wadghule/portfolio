FROM registry.gitlab.com/gaurav.wadghule/base-build:latest
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
CMD node --version
CMD ["npm","start"] 

